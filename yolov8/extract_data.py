# -*- coding: utf-8 -*-
"""
Created on Wed Aug 24 09:58:21 2023

@author: Morten Sahlertz
"""

import os
import yaml
import json
import argparse

from zipfile import ZipFile
from pathlib import Path
from sklearn.model_selection import train_test_split

def run_through_folder_and_delete_files(folder_path: Path):
    """
    Deletes all files within a specified folder.

    Args:
        folder_path (Path): The path to the folder to be cleared of files.

    Returns:
        None
    """
    for path, _, files in os.walk(folder_path):
        for name in files:
            os.remove(Path(path, name))

def run_through_list_of_files_from_temporary_folder_and_move(temporary_path: Path, move_folder_path: Path, list_of_images: list, list_of_labels: list):
    """
    Moves a list of image and label files from a temporary folder to a target folder.

    Args:
        temporary_path (Path): The path to the temporary folder containing the files to move.
        move_folder_path (Path): The path to the folder where the files will be moved.
        list_of_images (list): List of image filenames.
        list_of_labels (list): List of corresponding label filenames.

    Returns:
        None
    """
    assert len(list_of_images) == len(list_of_labels)
    for _ in range(len(list_of_images)):
            os.rename(Path(temporary_path, 'images', list_of_images[0]), Path(move_folder_path, 'images', list_of_images[0]))
            os.rename(Path(temporary_path, 'labels', list_of_labels[0]), Path(move_folder_path, 'labels', list_of_labels[0]))
            list_of_images.pop(0)
            list_of_labels.pop(0)

def run_through_zip_file_and_extract_data(zip_file_path: Path, remove_old_images: bool = True, random_state: int = 42):
    """
    Extracts data from a zip file, organizes it into training, test, and validation sets,
    updates custom data, and performs cleanup operations.

    Args:
        zip_file_path (Path): The path to the zip file containing the data to extract.
        remove_old_images (bool): Remove old images in the train, test and validation folders if set to true
        random_state (int): Random seed for reproducibility.

    Returns:
        None
    """
    # Check if zip file exits
    if os.path.exists(zip_file_path) and (zip_file_path.suffix == '.zip'):
        # Open the zip file, make a temporary folder and extract all the data to this folder
        with ZipFile(zip_file_path, 'r') as f:
            temporary_path = Path(zip_file_path.parent, 'temp')
            os.mkdir(temporary_path)
            f.extractall(temporary_path)
        # Run through the temporary folder and get all image and label filenames
        list_of_images = []
        list_of_labels = []
        for path, _, files in os.walk(temporary_path):
            for name in files:
                if Path(name).suffix.lower() == '.png' or Path(name).suffix.lower() == '.jpg' or Path(name).suffix.lower() == '.jpeg' or Path(name).suffix.lower() == '.tiff':
                    list_of_images.append(name)
                    list_of_labels.append(Path(name).stem + '.txt')

        # Run through the data folders and remove previous images and labels
        train_folder = Path(zip_file_path.parent, 'train')
        test_folder = Path(zip_file_path.parent, 'test')
        validation_folder = Path(zip_file_path.parent, 'val')

        if remove_old_images:
            run_through_folder_and_delete_files(train_folder)
            run_through_folder_and_delete_files(test_folder)
            run_through_folder_and_delete_files(validation_folder)
        
        # Generate a dataset with 70% training, 15% test and 15% validation data
        list_of_train_images, list_of_test_and_validation_images, list_of_train_labels, list_of_test_and_validation_labels = train_test_split(list_of_images, list_of_labels, test_size=0.3, random_state=random_state)
        list_of_test_images, list_of_validation_images, list_of_test_labels, list_of_validation_labels = train_test_split(list_of_test_and_validation_images, list_of_test_and_validation_labels, test_size=0.5, random_state=random_state)

        # Run through the temporary folder and move the files to their intended folders
        run_through_list_of_files_from_temporary_folder_and_move(temporary_path, train_folder, list_of_train_images, list_of_train_labels)
        run_through_list_of_files_from_temporary_folder_and_move(temporary_path, test_folder, list_of_test_images, list_of_test_labels)
        run_through_list_of_files_from_temporary_folder_and_move(temporary_path, validation_folder, list_of_validation_images, list_of_validation_labels)
        
        # Get the custom data from the YAML file
        with open(Path(train_folder.parent, 'custom_data.yaml').as_posix(), 'r') as file:
            custom_data = yaml.safe_load(file)

        # Extract the JSON data from the temporary files
        json_data = json.load(open(Path(temporary_path, 'notes.json')))
        names = []
        for category in json_data['categories']:
            names.append(category['name'])

        # Change the custom data in the YAML file and save it
        custom_data['names'] = names
        custom_data['nc'] = len(names)
        custom_data['train'] = train_folder.as_posix()
        custom_data['test'] = test_folder.as_posix()
        custom_data['val'] = validation_folder.as_posix()

        with open(Path(train_folder.parent, 'custom_data.yaml').as_posix(), 'w') as file:
            yaml.dump(custom_data, file)

        # Run through the temporary folder and remove files
        run_through_folder_and_delete_files(temporary_path)
        # Run through the temporary folder and remove the empty folders
        for path, _, files in os.walk(temporary_path):
            if Path(path) != temporary_path:
                os.rmdir(path)
        # Remove the temporary folder
        os.rmdir(temporary_path)

        # Print success message
        print('Zip file extracted and data moved to intended folders')

    else:
        print('Zip file does not exists!')


if __name__ == "__main__":
    
    parser = argparse.ArgumentParser()
    parser.add_argument('--zipfile', nargs=1, type=str, help='Zip folder downloaded from Label Studio. Must contain .zip suffix')
    opt = parser.parse_args()
    
    opt.zipfile = opt.zipfile[0]

    run_through_zip_file_and_extract_data(Path(Path(__file__).parent, opt.zipfile))