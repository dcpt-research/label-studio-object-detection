# -*- coding: utf-8 -*-
"""
Created on Thu Mar 9 09:58:21 2023

@author: Morten Sahlertz
"""

from ultralytics import YOLO

if __name__ == "__main__":
    # Choose the desire YOLO model
    model = YOLO("yolov8m.pt")
    # Train it with specified amount of epochs and image size, and based on the generated "custom_data.yaml" file
    model.train(data="custom_data.yaml", epochs=200, imgsz=640)
    # Finalized print statement
    print("Done!")