# Label-Studio Object Detection

## Description
This project uses the [Label Studio](https://github.com/HumanSignal/label-studio/tree/develop) from HumanSignal and the [YOLOv8](https://github.com/ultralytics/ultralytics) from Ultralytics to make a semi-automatic imaging labelling tool. The process starts with manually labelling images, but ends up with images being labelled automatically by a detection model from a [Docker](https://www.docker.com/) framework.

## Getting started

Clone the Label-Studio Object Detection gitlab repository. From the command line, run the following:

```
git clone https://gitlab.com/dcpt-research/label-studio-object-detection.git
```

## Installation

This is a three part installation, which should be followed chronologically:

- Label Studio installation
- YOLOv8 training
- ML backend for Label Studio

### Label Studio installation

Start out by making a new environment and installing the requirements from the corresponding requirements.txt file.

```
pip install -r requirements.txt
```

Now it is possible to run Label Studio, by calling the following command:

```
label-studio start
```

This will prompt a log in or sign up. If you already have a user account log in, otherwise use the sign up to make a user. Afterwards it is possible to create a dataset and start labelling this. For further information and explanation of the Label Studio GUI and how to label look at the [Label Studio Guide](https://labelstud.io/guide/).

### YOLOv8 training

When enough images have been labelled and the data is ready for training, export the data from Label Studio. Make sure to choose the YOLO format. When data is downloaded, put the corresponding zip file into the yolov8 part of this repository. When this is done, run the following command to extract the files from the zip compressed file, and move them to the correct folder, along with generating a 'custom_data.yaml' file, for the YOLOv8 training.

```
python extract_data.py --zipfile=name-of-zip-file-with-zip-suffix
```

The yolov8 folder should have the following directory tree structure:

``` bash
────yolov8
    ├───test
    │   ├───images
    │   └───labels
    ├───train
    │   ├───images
    │   └───labels
    └───val
        ├───images
        └───labels
```

The images folders, should now contain images and the labels should contain txt files.

Afterwards it is possible to train a YOLOv8 model, by simply using the following command:

```
python yolo_training.py
```

In the code is three easily changeable parameters:

```python
model = YOLO("yolov8m.pt") # This can be changed to different size models, current model size is (m)edium.

model.train(data="custom_data.yaml", epochs=200, imgsz=640) # This can be changed to other amounts of epochs and different image size.
```

Other parameters are changeable in these calls and for further documentation check [YOLOv8 Documentation](https://docs.ultralytics.com/).

### ML backend for Label Studio

For this part [Docker](https://www.docker.com/) is needed, if it is not already installed on your system you can download its installer from the aforementioned link.

First of, start with copying the 'best.pt' model from your YOLOv8 training to the label-studio-yolov8-backend folder. The 'best.pt' file should be located under the folowing directory (If a segmentation model has been trained instead of an object detection model, it will be a segment instead of a detect folder):

``` bash
────yolov8
    └───runs
        └───detect
            └───train
                └───weights
                    └───best.pt
```

Next is copying the API access token from Label Studio. This is accessable in Label Studio in the top right corner, by clicking the user icon and then choosing "Account & Settings". On this page it is visible right underneath the "Access token" headline. When found, it needs to copied into the model.py file on line 13 & line 16, which are shown below:

```python
LS_API_TOKEN = "COPY_API_TOKEN_HERE" # Make sure to copy API token here

API_KEY = get_env('API_KEY', 'COPY_API_TOKEN_HERE') # Make sure to copy API token here
```

It is also necessary to change the URL in the model.py file to that of your current host. This can be done by changing the URL on line 12, which is shown below:

```python
LS_URL = "http://10.60.8.15:8080" # Insert host URL here, with port 8080
```

Now it is possible to generate the docker image and containers by using the following command in the label-studio-yolov8-backend folder:

```
docker-compose up --build
```

This will generate the docker containers and start them. So now the ML backend should be running on localhost:9090. To test if it is up the following command can be used:

```
curl http://localhost:9090/health
```

Which should prompt the following response:

```
{"status":"UP"}
```

Next up is running Label Studio again. This time go the wanted project, click settings in the top right corner and go to Machine Learning. On this page click "Add Model". Add a model titel and give it http://localhost:9090/ as the URL. If you want to use it for interactive preannotations clicked this on before saving. It is also possible to add an optional description of the model. Now the semi-automatic imaging labelling should be ready and you can start labelling your images much faster.
