# YOLOv8 ML backend for the Label Studio

This project contains an ML backend for classifying in Label Studio. It uses the YOLOv8 model.

## Project Structure
### The repository contains the following files and directories:

- **Dockerfile**: The Dockerfile for building the backend container.

- **docker-compose.yml**: The docker-compose file for running the backend.

- **model.py**: The Python code for the ML backend model.

- **best.pt**: The pre-trained YOLOv8 model for classification.

- **uwsgi.ini**: The uWSGI configuration file for running the backend.

- **supervisord.conf**: The supervisord configuration file for running the backend processes.

- **requirements.txt**: The list of Python dependencies for the backend.


## Getting Started
1. Paste the best.pt model in the label-studio-yolov8-backend folder

2. Paste you Label Studio API key in ```model.py```

3. To use this backend, you'll need to have Docker and docker-compose installed. Then, run the following command to start the backend:

    ```docker-compose up```

This will start the backend on localhost:9090.

Check if it works:

```$ curl http://localhost:9090/health```

```{"status":"UP"}```

4. Connect running backend to Label Studio:

    ```label-studio start --init new_project --ml-backends http://localhost:9090```

5. Start the labeling process.

## Training
Model training is **not included** in this project. This will probably be added later.
