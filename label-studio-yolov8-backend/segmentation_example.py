import os
import random
import requests
from PIL import Image
from io import BytesIO

from ultralytics import YOLO
from label_studio_ml.model import LabelStudioMLBase
from label_studio_ml.utils import get_single_tag_keys, get_local_path, get_image_local_path, get_env

# URL with host
LS_URL = "http://10.60.8.15:8080" # Insert host URL here, with port 8080 
LS_API_TOKEN = "COPY_API_TOKEN_HERE" # Make sure to copy API token here

HOSTNAME = get_env('HOSTNAME', 'http://localhost:8080')
API_KEY = get_env('API_KEY', 'COPY_API_TOKEN_HERE') # Make sure to copy API token here

print('=> LABEL STUDIO HOSTNAME = ', HOSTNAME)
if not API_KEY:
    print('=> WARNING! API_KEY is not set')
# Initialize class inhereted from LabelStudioMLBase
class YOLOv8Model(LabelStudioMLBase):
    """
    A class representing a YOLOv8 model for object detection using Label Studio.

    Args:
        **kwargs: Additional keyword arguments.

    Attributes:
        from_name (str): The name of the 'from_name' field in the label config.
        to_name (str): The name of the 'to_name' field in the label config.
        value (str): The name of the 'value' field in the task data.
        classes (list): List of class names for the labels.
        labels (list): List of label names.
        model: YOLOv8 model instance.

    Methods:
        __init__(self, **kwargs):
            Initialize the YOLOv8Model instance.

        predict(self, tasks, **kwargs):
            Predict object locations in an image.

        fit(self, completions, workdir=None, **kwargs):
            Dummy function to train the model.
    """
    def __init__(self, **kwargs):
        """
        Initialize the YOLOv8Model instance.

        Args:
            **kwargs: Additional keyword arguments.

        Returns:
            None
        """
        # Call base class constructor
        super(YOLOv8Model, self).__init__(**kwargs)
        print('Initialized!')

        # Initialize self variables
        self.from_name, self.to_name, self.value, self.classes = get_single_tag_keys(
            self.parsed_label_config, 'PolygonLabels', 'Image')
        self.labels = ['capsules', 'tablets']
        # Load model
        self.model = YOLO("best.pt")

    # Function to predict
    def predict(self, tasks, **kwargs):
        """
        Predict object locations in an image.

        Args:
            tasks (list): List of tasks, each containing input data.

        Returns:
            list: List of predictions for the input tasks for 1 image.
        """
        task = tasks[0]

        # Getting URL of the image
        image_url = task['data'][self.value]
        full_url = LS_URL + image_url
        print("FULL URL: ", full_url)

        # Header to get request
        header = {
            "Authorization": "Token " + LS_API_TOKEN}
        
        print('First try!:')

        # Getting URL and loading image
        image = Image.open(BytesIO(requests.get(
            full_url, headers=header).content))
        print('Completed!')
        # Height and width of image
        original_width, original_height = image.size
        
        # Creating list for predictions and variable for scores
        predictions = []
        score = 0

        # Getting prediction using model
        results = self.model.predict(image)

        # Getting mask segments, boxes from model prediction
        for result in results:
            for i, (box, segm) in enumerate(zip(result.boxes, result.masks.segments)):
                
                # 2D array with poligon points 
                polygon_points = (segm * 100).tolist()

                # Adding dict to prediction
                predictions.append({
                    "from_name" : self.from_name,
                    "to_name" : self.to_name,
                    "id": str(i),
                    "type": "polygonlabels",
                    "score": box.conf.item(),
                    "original_width": original_width,
                    "original_height": original_height,
                    "image_rotation": 0,
                    "value": {
                        "points": polygon_points,
                        "polygonlabels": [self.labels[int(box.cls.item())]]
                    }})

                # Calculating score
                score += box.conf.item()

        print(10*"#", "Returned Prediction", 10*"#")

        # Dict with final dicts with predictions
        final_prediction = [{
            "result": predictions,
            "score": score / (i + 1),
            "model_version": "v8m"
        }]

        return final_prediction
    
    def fit(self, completions, workdir=None, **kwargs):
        """
        Dummy function to train the model.

        Args:
            completions (list): List of completions.
            workdir (str, optional): Path to the working directory. Defaults to None.

        Returns:
            dict: A dictionary containing a random number.
        """
        return {'random': random.randint(1, 10)}